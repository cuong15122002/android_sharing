package com.sharingapp.sharingfile.ui.home.home

import android.app.Notification
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.sharingapp.sharingfile.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import java.lang.reflect.Type
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
)  : BaseViewModel(){
    var listMedia = ArrayList<Uri>()
    val isData = MutableLiveData<Boolean>(false)

}