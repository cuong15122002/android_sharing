package com.sharingapp.sharingfile.data.model

import java.io.File

data class MediaFile(
    val imageId: String = "",
    val imageUri: String = "",
    val imageName: String = "",
    var isSelected: Boolean = false,
    var date: Long? = null,
    val type: Int? = null
)