package com.sharingapp.sharingfile.ui.home.home.bottom_sheet_select_file

enum class TypeFileSelect(val value: String){
    IMAGE_AND_VIDEO("image/*,video/*"),
    DOCUMENT("*/*"),
    HISTORY("history"),
    MORE("more"),
}