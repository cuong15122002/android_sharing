package com.sharingapp.sharingfile

import android.net.Uri
import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.io.File

object BindingAdapter {

    @JvmStatic
    @BindingAdapter("setView")
    fun TextView.setView(list : ArrayList<Uri>) {
        var name = ""
        list.forEach {
            val file = it.path?.let { it1 -> File(it1) }
            name += file?.name.toString() + "\n"
        }
        text = name
    }

}