package com.sharingapp.sharingfile.ui.home.home.bottom_sheet_select_file

import android.os.Bundle
import android.view.View
import com.sharingapp.sharingfile.BR
import com.sharingapp.sharingfile.R
import com.sharingapp.sharingfile.base.BaseBottomSheet
import com.sharingapp.sharingfile.databinding.BottomSheetSelectFileBinding
import javax.inject.Inject

class BottomSheetTypeFile @Inject constructor() : BaseBottomSheet<BottomSheetSelectFileBinding>(),
    TypeFileListener {
    override val layoutId: Int = R.layout.bottom_sheet_select_file
    var onClick :((type : TypeFileSelect) -> Unit)? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.setVariable(BR.listener, this@BottomSheetTypeFile)
        viewBinding.listener = this@BottomSheetTypeFile
        isCancelable = true
    }
    override fun pickImage() {
        onClick?.invoke(TypeFileSelect.IMAGE_AND_VIDEO)
    }

    override fun pickVideo() {
        onClick?.invoke(TypeFileSelect.HISTORY)
    }

    override fun pickDocument() {
        onClick?.invoke(TypeFileSelect.DOCUMENT)
    }

    override fun pickMore() {
        onClick?.invoke(TypeFileSelect.MORE)
    }
}
interface TypeFileListener {
    fun pickImage()
    fun pickVideo()
    fun pickDocument()
    fun pickMore()
}