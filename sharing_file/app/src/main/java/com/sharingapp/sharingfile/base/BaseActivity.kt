package com.sharingapp.sharingfile.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BaseActivity<VBD : ViewDataBinding, Model : BaseViewModel> : AppCompatActivity() {
    protected lateinit var viewBinding : VBD
    abstract val viewModel : Model
    protected abstract var layoutId : Int
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewBinding = DataBindingUtil.setContentView(this, layoutId)
    }

}