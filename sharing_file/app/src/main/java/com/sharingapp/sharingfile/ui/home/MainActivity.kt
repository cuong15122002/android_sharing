package com.sharingapp.sharingfile.ui.home

import android.os.Bundle
import androidx.activity.viewModels
import com.sharingapp.sharingfile.R
import com.sharingapp.sharingfile.base.BaseActivity
import com.sharingapp.sharingfile.base.BaseViewModel
import com.sharingapp.sharingfile.databinding.ActivityMainBinding
import com.sharingapp.sharingfile.ui.home.home.HomeFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity  : BaseActivity<ActivityMainBinding,BaseViewModel>() {
    override val viewModel: BaseViewModel by viewModels()
    override var layoutId: Int = R.layout.activity_main
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.beginTransaction().replace(R.id.frame_layout, HomeFragment())
            .commit()
        
    }

}