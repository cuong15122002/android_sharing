package com.sharingapp.sharingfile.ui.notification

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.CountDownTimer
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.sharingapp.sharingfile.R
import com.sharingapp.sharingfile.base.Constant
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.internal.notify
import javax.inject.Inject
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ShareNotification {
    @Provides
    @Singleton
    fun providesNotificationCompact(@ApplicationContext context: Context) : NotificationCompat.Builder {
        val noti = NotificationCompat.Builder(context,Constant.CHANNEL_NOTIFICATION_1)
            .setContentTitle("Đang tải file lên")
            .setContentText("1%")
            .setSmallIcon(R.drawable.ic_upload)
            .setProgress(100,0,false)
        return noti
    }


}