package com.sharingapp.sharingfile.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.sharingapp.sharingfile.BR

abstract class BaseFragment<VBD: ViewDataBinding, Model : BaseViewModel> : Fragment() {
    protected lateinit var viewBinding: VBD
    abstract val viewModel: Model
    abstract val layoutId: Int
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        viewBinding.lifecycleOwner = viewLifecycleOwner
        viewBinding.setVariable(BR.viewModel, viewModel)
        return viewBinding.root
    }
}