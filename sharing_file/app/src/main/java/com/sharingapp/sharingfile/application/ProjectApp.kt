package com.sharingapp.sharingfile.application

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import com.sharingapp.sharingfile.base.Constant
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ProjectApp : Application() {
    override fun onCreate() {
        super.onCreate()
        createChannelNotification()
    }

    fun createChannelNotification() {
        val channel = NotificationChannel(
            Constant.CHANNEL_NOTIFICATION_1,
            "Share",
            NotificationManager.IMPORTANCE_DEFAULT
        )
        val notificationManager = getSystemService(NotificationManager::class.java)
        notificationManager.createNotificationChannel(channel)
    }
}