package com.sharingapp.sharingfile.base

object Constant {
    const val TYPE_IMAGE =0
    const val TYPE_VIDEO =1
    const val TIME_OUT = 60L
    const val CHANNEL_NOTIFICATION_1 ="CHANNEL_NOTIFICATION_1"
}