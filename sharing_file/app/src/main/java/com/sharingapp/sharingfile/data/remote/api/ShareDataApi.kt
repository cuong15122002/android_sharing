package com.sharingapp.sharingfile.data.remote.api

import com.sharingapp.sharingfile.data.model.Path
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET

interface ShareDataApi {
    @GET(API.GET_PATH)
    suspend fun getPath() : Path

   // suspend fun postFile() : List<>

}

