package com.sharingapp.sharingfile.ui.home.home

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import com.sharingapp.sharingfile.R
import com.sharingapp.sharingfile.base.BaseFragment
import com.sharingapp.sharingfile.databinding.FragmentHomeBinding
import com.sharingapp.sharingfile.service.PostFileService
import com.sharingapp.sharingfile.ui.home.home.bottom_sheet_select_file.BottomSheetTypeFile
import com.sharingapp.sharingfile.ui.home.home.bottom_sheet_select_file.TypeFileSelect
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class HomeFragment @Inject constructor() : BaseFragment<FragmentHomeBinding, HomeViewModel>(), HomeListener {
    override val viewModel: HomeViewModel by viewModels()
    override val layoutId: Int = R.layout.fragment_home

    private val getContent = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data = result.data
            result.data?.let {
                val clipData = it.clipData
                if (clipData != null) {
                    for(i in 0 until clipData.itemCount)
                        viewModel.listMedia.add(clipData.getItemAt(i).uri)
                }
            }
        }
    }

    @Inject
    lateinit var bottomSheet: BottomSheetTypeFile
    private val pickMultipleMedia by lazy {
        registerForActivityResult(
            ActivityResultContracts.PickMultipleVisualMedia(400)
        )
        {
            val a :Int
            if (it.isEmpty()) {
                viewModel.listMedia.clear()
                viewModel.isData.postValue(false)
            } else {
                viewModel.isData.postValue(true)
                viewModel.listMedia.addAll(it)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.apply {
            listener = this@HomeFragment
            pickMultipleMedia
        }
        viewModel.isData.observe(viewLifecycleOwner) { isData ->
            if (isData) {
                viewBinding.viewModel = viewModel
            }
        }

    }

    override fun openSelectFile() {
        bottomSheet.apply {
            showDialog(this@HomeFragment.childFragmentManager, javaClass.name)
            onClick = { type ->
                when (type) {
                    TypeFileSelect.IMAGE_AND_VIDEO -> {
                        val pickMedia = Intent(Intent.ACTION_PICK)
                                        .setType(type.value)
                                        .putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                        getContent.launch(pickMedia)
                        startService()
                    }

                    TypeFileSelect.DOCUMENT -> {
                        val pickMedia = Intent(Intent.ACTION_GET_CONTENT)
                            .setType(type.value)
                            .putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                        getContent.launch(pickMedia)
                        startService()
                    }
                    TypeFileSelect.HISTORY -> {}
                    TypeFileSelect.MORE -> {}
                    else -> {}
                }
                this.dialog?.let { onDismiss(it) }
            }
        }

    }
    override fun cancle() {
        viewModel.isData.value = false
        val a = ArrayList<Int>()

    }
    fun startService() {
        val i = Intent(requireContext(),PostFileService::class.java)
        requireActivity().startService(i)
    }
}